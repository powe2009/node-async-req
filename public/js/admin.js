const delProd = (btn) => {
    const prodId = btn.parentNode.querySelector('[name=productId]').value;
    const csrf = btn.parentNode.querySelector('[name=_csrf]').value;
    const el = btn.closest('article');
    console.log('el', el);

    fetch('/admin/product/' + prodId, {
        method: 'DELETE',
        headers: {
            'csrf-token': csrf
        }
    })
    .then(response => {
        return response.json;
    })
    .then(data => {
        el.parentNode.removeChild(el);
    })
    .catch(error => {
        console.log('Error sending DELETE request to the server', error); 
    })
}